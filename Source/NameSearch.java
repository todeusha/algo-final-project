//@author Alex Todeush

import java.util.*;

public class NameSearch
{
    private static TernarySearchTree TST;
    private static BusStop[] busStops;

    NameSearch(BusStop[] busStops) {
        this.busStops = busStops;
    }

    public static void main() {
        TST = new TernarySearchTree();
        for (BusStop busStop : busStops) {
            TST.put(busStop.stopName(), busStop);
        }
        boolean useSystem = true;
        System.out.println("This feature will allow you to search for bus stops." +
                "\nTo use please enter the first few characters of the stops you are interested in." +
                "\nIf you are only interested in one bus stop enter its full name." +
                "\nWhen you are finished with this feature type exit.");
        while (useSystem) {
            System.out.print("Enter the name you are looking for (or exit): ");
            Scanner input = new Scanner(System.in);
            String userInput = input.nextLine();
            if (userInput.equalsIgnoreCase("exit")) {
                useSystem = false;
            } else {
                if (!userInput.trim().equals("")) {
                    try {
                        Iterable<String> matches = TST.keysWithPrefix(userInput.trim().toUpperCase());
                        int counter = 0;
                        for (Object i : matches) {
                            counter++;
                        }
                        if (counter == 0) {
                            System.out.println("No matching bus stops found.");
                        } else {
                            printMatches(matches, counter);
                        }
                    } catch (Exception e) {
                        System.out.println("Error during search.");
                    }
                } else {
                    System.out.println("No name entered.");
                }
            }
        }
        System.out.println("Name search mode exited.");
    }

    private static void printMatches(Iterable<String> matches, int numMatch) {
        Iterator value = matches.iterator();
        if (numMatch == 1) System.out.println("\nInformation of the " + numMatch + " matching bus stop:");
        else System.out.println("\nInformation of the " + numMatch + " matching bus stops:");
        while (value.hasNext()) {
            System.out.println(TST.get((String) value.next()));
        }
    }
}

//@author Alex Todeush

public class Trip
{
    private final int tripID;
    private final String arrivalTime;
    private final String departureTime;
    private final int stopID;
    private final int stopSequence;
    private final String stopHeadsign;
    private final int pickupType;
    private final int dropOff;
    private final double distTraveled;

    public Trip(int tripID, String arrivalTime, String departureTime, int stopID, int stopSequence, String stopHeadsign, int pickupType, int dropOff, double distTraveled) {
        this.tripID = tripID;
        this.arrivalTime = arrivalTime;
        this.departureTime = departureTime;
        this.stopID = stopID;
        this.stopSequence = stopSequence;
        this.stopHeadsign = stopHeadsign;
        this.pickupType = pickupType;
        this.dropOff = dropOff;
        this.distTraveled = distTraveled;
    }

    public String arrivalTime() {
        return arrivalTime;
    }

    @Override
    public String toString() {
        return "{Trip " + tripID + " Information; ID: " + tripID + ", Arrival Time: " + arrivalTime +
                "\nDeparture Time: " + departureTime +
                "\nStop ID: " + stopID + ", Stop Sequence: " + stopSequence + ", Stop Headsign: " + stopHeadsign +
                "\nPickup Type: " + pickupType + ", Drop Off: " + dropOff + ", Distance Traveled: " + distTraveled + "}";
    }
}

//@author Alex Todeush

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Stream;

public class BusManager
{

    private static EdgeWeightedDigraph Digraph;
    private static BusStop[] busStops;
    private static ArrayList<Trip> trips = new ArrayList<Trip>();

    public static void main(String[] args) {
        System.out.println("Reading in Bus Stop and trip data; this might take a while.");
        readStops();
        System.out.println("Stop data read (1/6)");
        Digraph = new EdgeWeightedDigraph(busStops.length);
        readTransfers();
        System.out.println("Transfer data read (2/6)");
        readStopTimes();
        System.out.println("Stop time data read 100% (6/6)");
        handleModeSelect();
    }

    private static void readStops() {
        int lineCount = 0;
        try (Stream<String> stream = Files.lines(Path.of("Inputs/stops.txt"), StandardCharsets.UTF_8)) {
            lineCount = (int) stream.count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        busStops = new BusStop[lineCount - 1];
        Digraph = new EdgeWeightedDigraph(lineCount - 1);
        try {
            File file = new File("Inputs/stops.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            br.readLine();
            for (int i = 0; i < lineCount - 1; i++) {
                line = br.readLine();
                tempArr = line.split(",");
                int stopCode = 0;
                try {
                    stopCode = Integer.parseInt(tempArr[1]);
                } catch (NumberFormatException e) {
                    //e.printStackTrace();
                }
                int parentStation = 0;
                if (tempArr.length == 10) {
                    try {
                        parentStation = Integer.parseInt(tempArr[1]);
                    } catch (NumberFormatException e) {
                        //e.printStackTrace();
                    }
                }
                String stopName = moveKeywords(tempArr[2]);
                BusStop stop = new BusStop(i, Integer.parseInt(tempArr[0]), stopCode, stopName, tempArr[3], Double.parseDouble(tempArr[4]), Double.parseDouble(tempArr[5]), tempArr[6], Integer.parseInt(tempArr[8]), parentStation);
                busStops[i] = stop;
            }
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static String moveKeywords(String name) {
        String[] keywords = {"FLAGSTOP", "WB", "NB", "SB", "EB"};
        for (int i = 0; i < keywords.length; i++) {
            if (name.contains(keywords[i])) {
                name = name.replace(keywords[i], "");
                name = name + " " + keywords[i];
            }
        }
        return name.trim();
    }

    private static void readTransfers() {
        int lineCount = 0;
        try (Stream<String> stream = Files.lines(Path.of("Inputs/transfers.txt"), StandardCharsets.UTF_8)) {
            lineCount = (int) stream.count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File file = new File("Inputs/transfers.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            br.readLine();
            for (int i = 0; i < lineCount - 1; i++) {
                line = br.readLine();
                tempArr = line.split(",");
                int fromID = Integer.parseInt(tempArr[0]);
                int toID = Integer.parseInt(tempArr[1]);
                int transferType = Integer.parseInt(tempArr[2]);
                double transferTime = 2;
                if (tempArr.length == 4 && transferType == 2) {
                    transferTime = Double.parseDouble(tempArr[3]);
                    transferTime /= 100;
                }
                BusStop from = null;
                BusStop to = null;
                if (fromID != toID) {
                    for (BusStop busStop : busStops) {
                        if (busStop.stopID() == fromID) from = busStop;
                        if (busStop.stopID() == toID) to = busStop;
                    }
                    DirectedEdge edge = new DirectedEdge(from, to, transferTime);
                    Digraph.addEdge(edge);
                }
            }
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static void readStopTimes() {
        int lineCount = 0;
        try (Stream<String> stream = Files.lines(Path.of("Inputs/stop_times.txt"), StandardCharsets.UTF_8)) {
            lineCount = (int) stream.count();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            File file = new File("Inputs/stop_times.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            String line;
            String[] tempArr;
            br.readLine();
            int previousTripID = 0;
            int fromID = 0;
            for (int i = 0; i < lineCount - 1; i++) {
                if (i == lineCount / 4) {
                    System.out.println("Stop time data read 25% (3/6)");
                }
                if (i == lineCount / 2) {
                    System.out.println("Stop time data read 50% (4/6)");
                }
                if (i == (lineCount * 3) / 4) {
                    System.out.println("Stop time data read 75% (5/6)");
                }
                line = br.readLine();
                tempArr = line.split(",");
                int tripID = Integer.parseInt(tempArr[0]);
                int toID = Integer.parseInt(tempArr[3]);
                if (previousTripID == tripID) {
                    BusStop from = null;
                    BusStop to = null;
                    for (BusStop busStop : busStops) {
                        if (busStop.stopID() == fromID) from = busStop;
                        if (busStop.stopID() == toID) to = busStop;
                    }
                    DirectedEdge edge = new DirectedEdge(from, to, 1);
                    Digraph.addEdge(edge);
                }
                previousTripID = tripID;
                fromID = toID;
                String arrivalTime = (tempArr[1]).trim();
                String departureTime = (tempArr[2]).trim();
                int stopSequence = Integer.parseInt(tempArr[4]);
                String stopHeadsign;
                try {
                    stopHeadsign = (tempArr[5]);
                } catch (Exception e) {
                    stopHeadsign = "";
                }
                int pickupType = Integer.parseInt(tempArr[6]);
                int dropOff = Integer.parseInt(tempArr[7]);
                double distTraveled;
                try {
                    distTraveled = Double.parseDouble(tempArr[8]);
                } catch (Exception e) {
                    distTraveled = 0;
                }
                String[] arrivalParts = arrivalTime.split(":");
                if (Integer.parseInt(arrivalParts[0].trim()) < 24) {
                    Trip trip = new Trip(tripID, arrivalTime, departureTime, toID, stopSequence, stopHeadsign, pickupType, dropOff, distTraveled);
                    trips.add(trip);
                }
            }
            br.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static void handleModeSelect() {
        boolean useSystem = true;
        int functionMode = 0;
        System.out.print("\n1. Shortest path between 2 bus stops\n2. Search for bus stops by name\n3. Searching for trips with given arrival time\nEnter number of desired function: ");
        while (useSystem) {
            Scanner input = new Scanner(System.in);
            try {
                String userInput = input.next();
                if (userInput.equalsIgnoreCase("exit")) {
                    useSystem = false;
                } else {
                    functionMode = Integer.parseInt(userInput);
                    if (functionMode > 3 || functionMode < 1) {
                        System.out.print("\nInvalid option, please enter the number 1-3 of your choice\n1. Shortest path between 2 bus stops\n2. Search for bus stops by name\n3. Searching for trips with given arrival time: ");
                    } else {
                        System.out.println("\nMode " + functionMode + " selected");
                        switch (functionMode) {
                            case 1:
                                ShortestPath shortest = new ShortestPath(busStops, Digraph);
                                shortest.main();
                                break;
                            case 2:
                                NameSearch nameSearch = new NameSearch(busStops);
                                nameSearch.main();
                                break;
                            case 3:
                                ArrivalTimesSearch arrivalTimesSearch = new ArrivalTimesSearch(trips);
                                arrivalTimesSearch.main();
                                break;
                        }
                        System.out.println("\nSelect another function or type \"exit\" to finish using the application.");
                        System.out.print("1. Shortest path between 2 bus stops\n2. Search for bus stops by name\n3. Searching for trips with given arrival time\nEnter number of desired function: ");
                    }
                }
            } catch (Exception e) {
                System.out.print("\nInvalid input, please enter the number of your choice\n1. Shortest path between 2 bus stops\n2. Search for bus stops by name\n3. Searching for trips with given arrival time: ");
            }
        }
    }
}
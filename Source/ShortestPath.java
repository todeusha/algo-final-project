//@author Alex Todeush

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;

public class ShortestPath
{
    private static EdgeWeightedDigraph Digraph;
    private static BusStop[] busStops;

    ShortestPath(BusStop[] busStops, EdgeWeightedDigraph Digraph) {
        this.busStops = busStops;
        this.Digraph = Digraph;
    }

    public static void main() {
        boolean useSystem = true;
        System.out.println("This feature will find the shortest path between 2 bus stops." +
                "\nTo use please enter the stop IDs of the two stops you are interested in." +
                "\nWhen you are finished with this feature type exit.");
        while (useSystem) {
            System.out.print("Enter the stop ID of your origin (or exit): ");
            int originID = getIDfromUser();
            if (originID != -1) {
                System.out.print("Enter the stop ID of your destination (or exit): ");
                int destinationID = getIDfromUser();
                if (destinationID != -1) {
                    if (originID == destinationID) {
                        System.out.println("Origin and destination are the same bus stop.");
                    } else {
                        Dijkstra Dijk = new Dijkstra(Digraph, stopByID(originID));
                        if (Dijk.distTo(stopByID(destinationID)) != Double.POSITIVE_INFINITY) {
                            System.out.println("\nThe cost of this journey is " + Dijk.distTo(stopByID(destinationID)) + ".");
                            Stack<DirectedEdge> pathTo = Dijk.pathTo(stopByID(destinationID));
                            System.out.println("The stops on this journey are: ");
                            printPath(pathTo);
                        } else {
                            System.out.println("There is no route between these two stations.");
                        }
                    }
                } else {
                    useSystem = false;
                }
            } else {
                useSystem = false;
            }
        }
        System.out.println("Shortest path mode exited.");
    }

    public static void printPath(Stack<DirectedEdge> pathTo) {
        Iterator stops = pathTo.iterator();
        ArrayList<DirectedEdge> edges = new ArrayList<>();
        ArrayList<BusStop> pathStops = new ArrayList<>();
        while (stops.hasNext()) {
            edges.add((DirectedEdge) stops.next());
        }
        for (int i = 0; i < edges.size(); i++) {
            boolean addFrom = true;
            boolean addTo = true;
            for (int j = 0; j < pathStops.size(); j++) {
                if (pathStops.get(j).stopID() == edges.get(i).fromStop().stopID()) {
                    addFrom = false;
                }
                if (pathStops.get(j).stopID() == edges.get(i).toStop().stopID()) {
                    addTo = false;
                }
            }
            if (addFrom) pathStops.add(edges.get(i).fromStop());
            if (addTo) pathStops.add(edges.get(i).toStop());
        }
        for (int i = pathStops.size() - 1; i > 0; i--) {
            System.out.println(pathStops.get(i));
        }
    }

    public static int stopByID(int stopID) {
        for (int i = 0; i < busStops.length; i++) {
            if (stopID == busStops[i].stopID()) {
                return busStops[i].number();
            }
        }
        return -1;
    }

    public static int getIDfromUser() {
        int stopID = -1;
        while (true) {
            Scanner input = new Scanner(System.in);
            String userInput = input.nextLine().trim();
            if (userInput.equalsIgnoreCase("exit")) {
                return -1;
            } else {
                if (!userInput.equals("")) {
                    try {
                        stopID = Integer.parseInt(userInput);
                        if (stopIDValid(stopID)) {
                            return stopID;
                        } else {
                            System.out.print("Invalid input, this stop ID does not exist. Please try again: ");
                        }
                    } catch (Exception e) {
                        System.out.print("Invalid input, please enter the numeric stop ID of your choice: ");
                    }
                } else {
                    System.out.println("No ID entered.");
                    System.out.print("Enter the ID you are looking for (or exit): ");
                }
            }
        }
    }

    public static boolean stopIDValid(int stopID) {
        for (int i = 0; i < busStops.length; i++) {
            if (stopID == busStops[i].stopID()) {
                return true;
            }
        }
        return false;
    }
}

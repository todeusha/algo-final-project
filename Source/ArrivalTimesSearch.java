//@author Alex Todeush

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Pattern;

public class ArrivalTimesSearch
{
    private static TernarySearchTree TST;
    private static ArrayList<Trip> trips = new ArrayList<>();

    ArrivalTimesSearch(ArrayList<Trip> trips) {
        this.trips = trips;
    }

    public static void main() {
        TST = new TernarySearchTree();
        for (int i = 0; i < trips.size(); i++) {
            ArrayList<Trip> before;
            before = (ArrayList<Trip>) TST.get(trips.get(i).arrivalTime());
            if (before == null) {
                before = new ArrayList<Trip>();
            }
            before.add(trips.get(i));
            TST.put(trips.get(i).arrivalTime(), before);
        }

        boolean useSystem = true;
        System.out.println("This feature will allow you to search for trips by their arrival time." +
                "\nTo use please enter the arrival time of the trips you are interested in." +
                "\nPlease enter times in the following format (hh:mm:ss)." +
                "\nWhen you are finished with this feature type exit.");
        while (useSystem) {
            System.out.print("Enter the time you are looking for (or exit): ");
            Scanner input = new Scanner(System.in);
            String userInput = input.nextLine();
            if (userInput.equalsIgnoreCase("exit")) {
                useSystem = false;
            } else {
                if (!userInput.trim().equals("")) {
                    try {
                        boolean matchHHMMSS = Pattern.matches("\\d{1,2}:\\d{2}:\\d{2}", userInput);
                        if (!matchHHMMSS) {
                            System.out.println("Error during search. Enter the target trip time in the format (hh:mm:ss).");
                        } else {
                            String[] parts = userInput.trim().split(":");
                            if (parts[0].charAt(0) == '0') {
                                userInput = String.valueOf(parts[0].charAt(1)) + ':' + parts[1] + ':' + parts[2];
                            }
                            ArrayList<Trip> matches = (ArrayList<Trip>) TST.get(userInput.trim());
                            int counter = 0;
                            if (matches != null) {
                                for (Object i : matches) {
                                    counter++;
                                }
                            }
                            if (counter == 0) {
                                System.out.println("No matching trips found.");
                            } else {
                                printMatches(matches);
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Error during search. Enter the target trip time in the format (hh:mm:ss).");
                    }
                } else {
                    System.out.println("No time entered.");
                }
            }
        }
        System.out.println("Arrival time search mode exited.");
    }

    private static void printMatches(ArrayList<Trip> matches) {
        if (matches.size() == 1) System.out.println("\nInformation of the one matching trip:");
        else System.out.println("\nInformation of the " + matches.size() + " matching bus trips:");
        for (int i = 0; i < matches.size(); i++) {
            System.out.println(matches.get(i).toString());
        }
    }
}

//@author Alex Todeush
//Class adapted from Algorithms (4th Edition), Robert Sedgewick and Kevin Wayne, Pearson Education, 2011.

public class DirectedEdge
{
    private final BusStop from;
    private final BusStop to;
    private final double weight;


    public DirectedEdge(BusStop from, BusStop to, double weight) {
        if (Double.isNaN(weight)) throw new IllegalArgumentException("Weight is NaN");
        this.from = from;
        this.to = to;
        this.weight = weight;
    }

    public BusStop fromStop() {
        return from;
    }

    public BusStop toStop() {
        return to;
    }

    public int from() {
        return from.number();
    }

    public int to() {
        return to.number();
    }

    public double weight() {
        return weight;
    }

    @Override
    public String toString() {
        return "DirectedEdge{" + " From: " + from + "\nTo: " + to + "\nWeight = " + weight + '}';
    }

}

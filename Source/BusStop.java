//@author Alex Todeush

public class BusStop
{
    private final int number;
    private final int stopID;
    private final int stopCode;
    private final String stopName;
    private final String stopDesc;
    private final double stopLat;
    private final double stopLon;
    private final String zoneID;
    private final int locationType;
    private final int parentStation;


    public BusStop(int number, int stopID, int stopCode, String stopName, String stopDesc, double stopLat, double stopLon, String zoneID, int locationType, int parentStation) {
        this.number = number;
        this.stopID = stopID;
        this.stopCode = stopCode;
        this.stopName = stopName;
        this.stopDesc = stopDesc;
        this.stopLat = stopLat;
        this.stopLon = stopLon;
        this.zoneID = zoneID;
        this.locationType = locationType;
        this.parentStation = parentStation;

    }

    public String stopName() {
        return stopName;
    }

    public int number() {
        return number;
    }

    public int stopID() {
        return stopID;
    }

    @Override
    public String toString() {
        return "{Stop " + stopID + " Information; ID: " + stopID + ", Code: " + stopCode +
                "\nName: " + stopName +
                "\nDescription: " + stopDesc + ", Latitude: " + stopLat + ", Longitude: " + stopLon +
                "\nZone ID: " + zoneID + ", Location Type: " + locationType + ", Parent Station: " + parentStation + "}";
    }
}
